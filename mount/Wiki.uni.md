# Creating
```
mkfs.btrfs -L now /dev/sdb1
mount /dev/sdb1 /mnt
cd /mnt
btrfs sub create .FS
cd .FS/
btrfs sub create Downloads
cd
umount /mnt
mkdir -p /now
```


# entire Volume
```
UUID=ef0751fa-d909-42ea-8b87-5cb137d22602   /now  btrfs  defaults  0  0
```

# Subvol
```
UUID=ef0751fa-d909-42ea-8b87-5cb137d22602   /home/me/Downloads  btrfs subvol=/.FS/Downloads,noatime  0  0
```

https://btrfs.wiki.kernel.org/index.php/SysadminGuide